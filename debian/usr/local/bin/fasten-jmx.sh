#!/usr/bin/env bash

set -e

LANG=C
PATH="/bin:/usr/sbin:/usr/bin:/sbin:/usr/local/bin:/usr/local/sbin:$PATH"

type -P java >/dev/null || exit 1

java -jar "/opt/fasten/lib/jmx-proxy.jar" "${@:?}"
# install jboss-client.jar to local repository
mvn install:install-file \
-Dfile=/opt/wildfly/bin/client/jboss-client.jar \
-DgroupId=org.wildfly \
-DartifactId=jboss-client \
-Dversion=8.2.0.Final \
-Dpackaging=jar \
-DgeneratePom=true

# dep packaging from directory src/debian
dpkg --build debian fasten-jmx-8.2.0.final.deb

# executions

## single attribute
java -jar target/jmx-proxy-0.2.0-SNAPSHOT-jar-with-dependencies.jar \
localhost 9990 admin admin org.ustaxi.app.infra.jmx.mbean.version:name=billing-app-ear,type=ApplicationVersionMXBeanImpl ApplicationVersion

## multiple attributes and credentials from json
java -jar target/jmx-proxy-0.2.0-SNAPSHOT-jar-with-dependencies.jar src/main/resources/example.json
package com.fasten;

/**
 * @author fil
 */
public class Attribute {

    private String bean;
    private String name;
    private String value;

    public Attribute(String bean, String name, String value){
        setBean(bean);
        setName(name);
        setValue(value);
    }

    public String getBean() {
        return bean;
    }

    public void setBean(String bean) {
        this.bean = bean;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

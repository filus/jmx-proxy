package com.fasten;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.*;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author fil
 */
public class Main {

    private interface SilentCall<T> {
        T call();
    }

    public static void main(final String[] args) {
        try {
            if (args.length < 1) {
                throw new FException("Arguments is missed!");
            }
            String ret;
            if (args.length > 1) {
                final String host = args[0];
                final String port = args[1];
                final String username = args[2];
                final String password = args[3];
                final String fullDomain = args[4];
                final String attribute = args[5];
                ret = disableOut(new SilentCall<String>() {
                    @Override
                    public String call() {
                        return new Main().process(host, port, username, password, fullDomain, attribute);
                    }
                });
            } else {
                ret = disableOut(new SilentCall<String>() {
                    @Override
                    public String call() {
                        return new Main().process(new File(args[0]));
                    }
                });
            }
            System.out.println(ret);
        } catch (FException e) {
            System.out.println("{\"error\":\"" + e.getMessage() + "\"}");
        }
    }

    private static <T> T disableOut(SilentCall<T> call) {
        PrintStream out = System.out;
        PrintStream err = System.err;
        PrintStream silent = new PrintStream(new OutputStream() {
            @Override
            public void write(int b) throws IOException {
            }
        });
        System.setOut(silent);
        System.setErr(silent);
        try {
            return call.call();
        } finally {
            System.setOut(out);
            System.setErr(err);
        }
    }

    private String process(File requestFile) throws FException {
        if (requestFile == null || !requestFile.exists()) {
            throw new FException("request body file not defined");
        }
        ObjectMapper mapper = new ObjectMapper();
        Request req;
        try {
            String body = IOUtils.toString(new FileInputStream(requestFile));
            req = mapper.readValue(body, Request.class);
        } catch (IOException e) {
            throw new FException("broken json format");
        }

        Map<String, Object> env = new HashMap<>();
        env.put(JMXConnector.CREDENTIALS, new String[]{req.getUsername(), req.getPassword()});
        String urlString = "service:jmx:http-remoting-jmx://" + req.getHost() + ":" + req.getPort();
        JMXServiceURL url;
        try {
            url = new JMXServiceURL(urlString);
        } catch (MalformedURLException e) {
            throw new FException("bad formed url string: " + urlString);
        }
        try (JMXConnector connector = JMXConnectorFactory.connect(url, env)) {
            MBeanServerConnection server = connector.getMBeanServerConnection();
            List<Attribute> attributes = new ArrayList<>();
            for(Item item : req.getItems()){
                attributes.addAll(getAttributes(server, item));
            }
            return mapper.writeValueAsString(attributes);
        } catch (IOException e) {
            throw new FException("connection failure to address: " + urlString);
        }
    }

    private String process(String host, String port, String username, String password,
                           String fullDomain, String attribute)
            throws FException {
        Map<String, Object> env = new HashMap<>();
        env.put(JMXConnector.CREDENTIALS, new String[]{username, password});
        String urlString = "service:jmx:http-remoting-jmx://" + host + ":" + port;
        JMXServiceURL url;
        try {
            url = new JMXServiceURL(urlString);
        } catch (MalformedURLException e) {
            throw new FException("bad formed url string: " + urlString);
        }
        try (JMXConnector connector = JMXConnectorFactory.connect(url, env)) {
            MBeanServerConnection server = connector.getMBeanServerConnection();
            return String.valueOf(getAttribute(server, fullDomain, attribute));
        } catch (IOException e) {
            throw new FException("connection failure to address: " + urlString + ", " + e.getMessage(), e);
        }
    }

    private List<Attribute> getAttributes(MBeanServerConnection server, Item item) {
        String objectNameString = item.getDomain() + ":";
        if (StringUtils.isNotEmpty(item.getName())) {
            objectNameString += "name=" + item.getName() + ",";
        }
        objectNameString += "type=" + item.getType();
        List<Attribute> ret = new ArrayList<>();
        for (String attrName : item.getAttributes()) {
            Object value = getAttribute(server, objectNameString, attrName);
            ret.add(new Attribute(objectNameString, attrName, value == null ? null : value.toString()));
        }
        return ret;
    }

    private Object getAttribute(MBeanServerConnection server, String objectNameString, String attrName) {
        ObjectName objectName;
        try {
            objectName = new ObjectName(objectNameString);
        } catch (Exception e) {
            throw new FException("cant found MXObject: " + objectNameString + ", " + e.getMessage(), e);
        }
        try {
            return server.getAttribute(objectName, attrName);
        } catch (Exception e) {
            throw new FException("cant read attribute: " + attrName + " from bean: " + objectNameString, e);
        }
    }

}

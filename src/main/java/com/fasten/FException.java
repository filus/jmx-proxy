package com.fasten;

/**
 * @author fil
 */
public class FException extends RuntimeException {

    public FException(){}

    public FException(String msg){
        super(msg);
    }

    public FException(String msg, Throwable e){
        super(msg, e);
    }

}

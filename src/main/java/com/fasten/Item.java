package com.fasten;

import java.util.Collections;
import java.util.List;

/**
 * @author fil
 */
public class Item {

    private String domain;
    private String name;
    private String type;
    private List<String> attributes;

    public Item(){}

    public Item(String domain, String name, String type, String attribute){
        setDomain(domain);
        setName(name);
        setType(type);
        setAttributes(Collections.singletonList(attribute));
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName()+"{" +
                "domain='" + domain + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", attributes=" + attributes +
                '}';
    }
}
